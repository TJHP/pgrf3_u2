#version 440

layout(location = 5) in vec3 outCol;
out vec4 outColor;
uniform int colorType;

void main() {

	if(colorType == 0){
		outColor = vec4(1.0);
	}
	if(colorType == 1){
		outColor = vec4(outCol.xyz, 1.0);
	}

} 

