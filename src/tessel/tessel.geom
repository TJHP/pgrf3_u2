#version 440

layout(triangles, invocations = 3) in;
layout(triangle_strip, max_vertices = 3) out;
layout(location = 1) in vec3 positionTese[];
layout(location = 2) in vec3 distance[];
layout(location = 3) out vec3 normal;
layout(location = 4) out vec3 triDist;
layout(location = 5) out vec3 outCol;

void main() {

	vec3 X = positionTese[1] - positionTese[0];
	vec3 Y = positionTese[2] - positionTese[0];
	normal = normalize(cross(X, Y));

	outCol = vec3(1,0,0);
	normal = distance[0];
	triDist = vec3(1, 0, 0);
	gl_Position = gl_in[0].gl_Position;
	EmitVertex();

	outCol = vec3(0,1,0);
	normal = distance[1];
	triDist = vec3(0, 1, 0);
	gl_Position = gl_in[1].gl_Position;
	EmitVertex();

	outCol = vec3(0,0,1);
	normal = distance[2];
	triDist = vec3(0, 0, 1);
	gl_Position = gl_in[2].gl_Position;
	EmitVertex();

	EndPrimitive();
}
