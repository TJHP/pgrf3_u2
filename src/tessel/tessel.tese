#version 430 core
layout (triangles, equal_spacing, ccw) in;              

layout(location = 1) in vec3 positionTesc[];
layout(location = 1) out vec3 positionTese;
layout(location = 2) out vec3 distance; 

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

void main(void){

    vec3 a = gl_TessCoord.x * positionTesc[0];
    vec3 b = gl_TessCoord.y * positionTesc[1];
    vec3 c = gl_TessCoord.z * positionTesc[2];
    
    distance = gl_TessCoord;
    positionTese = normalize(a + b + c);
    
    gl_Position = proj*view*model*vec4(positionTese, 1);
}