#version 430 core

layout (vertices = 3) out;
layout(location = 1) in vec3 positionVert[];
layout(location = 1) out vec3 positionTesc[];

uniform float time;
uniform int tessType;

void main(void){
positionTesc[gl_InvocationID] = positionVert[gl_InvocationID];

	if(tessType == 0){
   		if (gl_InvocationID == 0){
        	gl_TessLevelInner[0] = time; 
        	gl_TessLevelOuter[0] = 1.0;
        	gl_TessLevelOuter[1] = 1.0;
        	gl_TessLevelOuter[2] = 1.0;
    	}
    }
    
    if(tessType == 1){
   		if (gl_InvocationID == 0){
        	gl_TessLevelInner[0] = time; 
        	gl_TessLevelOuter[0] = time;
        	gl_TessLevelOuter[1] = time;
        	gl_TessLevelOuter[2] = time;
    	}
    }
    
        if(tessType == 2){
   		if (gl_InvocationID == 0){
        	gl_TessLevelInner[0] = time; 
        	gl_TessLevelOuter[0] = 2.0;
        	gl_TessLevelOuter[1] = time;
        	gl_TessLevelOuter[2] = 2.0;
    	}
    }
    
        if(tessType == 3){
   		if (gl_InvocationID == 0){
        	gl_TessLevelInner[0] = time; 
        	gl_TessLevelOuter[0] = time;
        	gl_TessLevelOuter[1] = 3.0;
        	gl_TessLevelOuter[2] = 3.0;
    	}
    }
    
}
