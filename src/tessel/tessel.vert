#version 440

layout(location = 1) in vec3 inPosition;
layout(location = 1) out vec3 positionVert;

uniform vec3 viewPos;
uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;
uniform float time;

void main() {
	vec3 position = inPosition;
	position.y = position.y + 0.1;
	positionVert = inPosition;

	gl_Position = proj*view*model*vec4(position.xyz, 1.0);
} 
