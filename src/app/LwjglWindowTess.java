package app;
import org.lwjgl.glfw.*;
import org.lwjgl.opengl.*;
import org.lwjgl.system.*;
import java.nio.*;
import java.util.concurrent.CountDownLatch;
import static org.lwjgl.glfw.Callbacks.*;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.system.MemoryStack.*;
import static org.lwjgl.system.MemoryUtil.*;
/**
* T��da staraj�c� se o vytvo�en� druh�ho vl�kna pro okno s nab�dkou (P�evzato z uk�zek LWJGL-Samples UHK FIM)
* @author Tom� Poho�sk�
*/
public class LwjglWindowTess extends Thread {

	public static final int WIDTH = 1200;
    public static final int HEIGHT = 800;
	private long window;
	private Renderer renderer;
	private CountDownLatch quit;
	private boolean init = true;
    private int index;
    private static final boolean DEBUG = true;

	public long getWindow() {
		return window;
	}

	static {
        if (DEBUG) {
            System.setProperty("org.lwjgl.util.Debug", "true");
            System.setProperty("org.lwjgl.util.NoChecks", "false");
            System.setProperty("org.lwjgl.util.DebugLoader", "true");
            System.setProperty("org.lwjgl.util.DebugAllocator", "true");
            System.setProperty("org.lwjgl.util.DebugStack", "true");
            Configuration.DEBUG_MEMORY_ALLOCATOR.set(true);
        }
    }

    public LwjglWindowTess(int index, CountDownLatch quit, Renderer renderer){
    	this.index = index;
        this.quit = quit;
        this.renderer = renderer;
		init();
	}
	
	private void init() {
		String text = "U2 PGRF3 UHK FIM 2019";
		// Create the window
		window = glfwCreateWindow(WIDTH, HEIGHT, text, NULL, NULL); 
		if ( window == NULL )
			throw new RuntimeException("Failed to create the GLFW window");

		glfwSetKeyCallback(window, renderer.getKeyCallback());
		glfwSetWindowSizeCallback(window,renderer.getWsCallback());
		glfwSetMouseButtonCallback(window,renderer.getMouseCallback());
		glfwSetCursorPosCallback(window,renderer.getCursorCallback());
		glfwSetScrollCallback(window,renderer.getScrollCallback());
		
		try ( MemoryStack stack = stackPush() ) {
			IntBuffer pWidth = stack.mallocInt(1); 
			IntBuffer pHeight = stack.mallocInt(1); 

			glfwGetWindowSize(window, pWidth, pHeight);

			GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());

			glfwSetWindowPos(
				window,
				(vidmode.width() - pWidth.get(0)) / 2 + index * 30,
				(vidmode.height() - pHeight.get(0)) / 2 + index * 30
			);
		} 
		glfwShowWindow(window);	
	}
	
	public void dispose() {
		renderer.dispose();
		glfwFreeCallbacks(window);
		glfwDestroyWindow(window);
	}
	
	@Override
	public void run() {
		glfwMakeContextCurrent(window);
		GL.createCapabilities();

		glfwSwapInterval(1);

		if (init){ 
			renderer.init();
			init = false;
		}
		
		while (quit.getCount() != 0) {
        	renderer.display();
			glfwSwapBuffers(window);
			glfwPollEvents();
        }
        GL.setCapabilities(null);
 	}
	
	
	public void setProj(boolean proj) {
		renderer.setProj(proj);
	}
	public void setLineFill(boolean liFi) {
		renderer.setLineFill(liFi);
	}
	public void setLineWidth(int width) {
		renderer.setLineWidth(width);
	}
	public void setMovings(boolean type) {
		renderer.setMovings(type);
	}
	public void setCol(int col) {
		renderer.setCol(col);
	}  
	public void tessType(int type) {
		renderer.tessType(type);
	}  
	public void tessMode(int type) {
		renderer.tessMode(type);
	} 
	public void setSpeed(int speed) {
		renderer.setSpeed(speed);
	}
}