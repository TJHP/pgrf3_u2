package app;


import static lwjglutils.ShaderUtils.TESSELATION_SUPPORT_VERSION;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.glDeleteProgram;
import static org.lwjgl.opengl.GL20.glGetUniformLocation;
import static org.lwjgl.opengl.GL20.glUniform1f;
import static org.lwjgl.opengl.GL20.glUniformMatrix4fv;
import static org.lwjgl.opengl.GL20.glUniform1i;
import static org.lwjgl.opengl.GL20.glUseProgram;
import static org.lwjgl.opengl.GL40.GL_MAX_PATCH_VERTICES;
import static org.lwjgl.opengl.GL40.GL_PATCHES;
import static org.lwjgl.opengl.GL40.GL_PATCH_VERTICES;
import static org.lwjgl.opengl.GL40.glPatchParameteri;

import java.nio.DoubleBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.GLFWCursorPosCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWMouseButtonCallback;
import org.lwjgl.glfw.GLFWScrollCallback;
import org.lwjgl.glfw.GLFWWindowSizeCallback;

import app.AbstractRenderer;
import lwjglutils.OGLBuffers;
import lwjglutils.OGLTextRenderer;
import lwjglutils.OGLUtils;
import lwjglutils.ShaderUtils;
import transforms.Camera;
import transforms.Mat4;
import transforms.Mat4OrthoRH;
import transforms.Mat4PerspRH;
import transforms.Mat4Scale;
import transforms.Vec3D;


/**
*  T��da obsahuj�c� hlavn� funkcionality
*/
public class Renderer extends AbstractRenderer{

	private int shaderProgram;
	private int locMatModel, locMatView, locMatProj, locTime, locTessType, locColType;
	private int lineWidth = 0;
	private int tessType = 0;
	private int colorType = 0;
	private int tessMode = 0;
	private int speed;
	private float time = 1;
	private double ox, oy;
	private boolean demoTypeChanged = true;
	private boolean stop = false;
	private boolean mouseButton1 = false;
	private boolean projSwitch = true;
	private boolean lineFillSwitch = false;
	private OGLBuffers buffers;
	private Camera cam = new Camera();
	private Mat4 projPersp = new Mat4PerspRH(Math.PI / 4, 1, 0.1, 100.0);	
	private Mat4 projOrth = new Mat4OrthoRH(20, 20, 1, 200);
	
	private GLFWKeyCallback   keyCallback = new GLFWKeyCallback() {
		@Override
		public void invoke(long window, int key, int scancode, int action, int mods) {
			if ( key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE )
				glfwSetWindowShouldClose(window, true);
			if (action == GLFW_PRESS || action == GLFW_REPEAT){
				switch (key) {
				case GLFW_KEY_W:
					cam = cam.forward(1);
					break;
				case GLFW_KEY_D:
					cam = cam.right(1);
					break;
				case GLFW_KEY_S:
					cam = cam.backward(1);
					break;
				case GLFW_KEY_A:
					cam = cam.left(1);
					break;
				case GLFW_KEY_E:
					cam = cam.down(1);
					break;
				case GLFW_KEY_Q:
					cam = cam.up(1);
					break;
				case GLFW_KEY_SPACE:
					cam = cam.withFirstPerson(!cam.getFirstPerson());
					break;
				}
			}
		}
	};
    
    private GLFWWindowSizeCallback wsCallback = new GLFWWindowSizeCallback() {
    	@Override
    	public void invoke(long window, int w, int h) {
            if (w > 0 && h > 0 && (w != width || h != height)) {
            	width = w;
            	height = h;
            	projPersp = new Mat4PerspRH(Math.PI / 4, height / (double) width, 0.1, 100.0);
            	if (textRenderer != null)
            		textRenderer.resize(width, height);

            }
        }
    };
    
    private GLFWMouseButtonCallback mbCallback = new GLFWMouseButtonCallback () {
    	@Override
		public void invoke(long window, int button, int action, int mods) {
			if (button==GLFW_MOUSE_BUTTON_1 && action == GLFW_PRESS){
				mouseButton1 = true;
				DoubleBuffer xBuffer = BufferUtils.createDoubleBuffer(1);
				DoubleBuffer yBuffer = BufferUtils.createDoubleBuffer(1);
				glfwGetCursorPos(window, xBuffer, yBuffer);
				ox = xBuffer.get(0);
				oy = yBuffer.get(0);
			}
			
			if (button==GLFW_MOUSE_BUTTON_1 && action == GLFW_RELEASE){
				mouseButton1 = false;
				DoubleBuffer xBuffer = BufferUtils.createDoubleBuffer(1);
				DoubleBuffer yBuffer = BufferUtils.createDoubleBuffer(1);
				glfwGetCursorPos(window, xBuffer, yBuffer);
				double x = xBuffer.get(0);
				double y = yBuffer.get(0);
				cam = cam.addAzimuth((double) Math.PI * (ox - x) / width)
        				.addZenith((double) Math.PI * (oy - y) / width);
				ox = x;
				oy = y;
        	}
		}
	};
	
	private GLFWCursorPosCallback cpCallbacknew = new GLFWCursorPosCallback() {
    	@Override
        public void invoke(long window, double x, double y) {
			if (mouseButton1) {
				cam = cam.addAzimuth((double) Math.PI * (ox - x) / width)
						.addZenith((double) Math.PI * (oy - y) / width);
				ox = x;
				oy = y;
			}
    	}
    };
    
	private GLFWScrollCallback scrollCallback = new GLFWScrollCallback() {
		@Override
		public void invoke(long window, double dx, double dy) {
			if (dy < 0)
				cam = cam.mulRadius(0.9f);
			else
				cam = cam.mulRadius(1.1f);
		}
	};
    
  	@Override
	public GLFWKeyCallback getKeyCallback() {
		return keyCallback;
	}

	@Override
	public GLFWWindowSizeCallback getWsCallback() {
		return wsCallback;
	}

	@Override
	public GLFWMouseButtonCallback getMouseCallback() {
		return mbCallback;
	}

	@Override
	public GLFWCursorPosCallback getCursorCallback() {
		return cpCallbacknew;
	}

	@Override
	public GLFWScrollCallback getScrollCallback() {
		return scrollCallback;
	}
	
	void createBuffers() {
		int[] indexBufferData = {
				 2,1,0,2, 2,0,3,2, 2,1,6,2, 2,6,7,2, 2,7,3,2, 2,2,3,4,
				 4,3,0,4, 4,0,5,4, 4,3,8,4, 4,8,9,4, 4,9,5,4, 4,4,4,5,
				 5,9,10,5, 5,5,5,10, 
				 10,11,9,10, 10,10,10,9,
				 9,11,8,9, 9,9,9,8,
				 8,3,7,8, 8,11,7,8, 8,8,8,7, 
				 7,11,6,7, 7,7,7,6,
				 6,1,10,6, 6,11,10,6, 6,6,6,1,
				 1,0,5,1, 1,5,10,1
		};
		
		float[] vertexBufferDataPos = {
			     0.000f,  0.000f,  1.000f,
		         0.894f,  0.000f,  0.447f,
		         0.276f,  0.851f,  0.447f,
		        -0.724f,  0.526f,  0.447f,
		        -0.724f, -0.526f,  0.447f,
		         0.276f, -0.851f,  0.447f,
		         0.724f,  0.526f, -0.447f,
		        -0.276f,  0.851f, -0.447f,
		        -0.894f,  0.000f, -0.447f,
		        -0.276f, -0.851f, -0.447f,
		         0.724f, -0.526f, -0.447f,
		         0.000f,  0.000f, -1.000f 			
		};
		OGLBuffers.Attrib[] attributesPos = {
				new OGLBuffers.Attrib("inPosition", 3),
		};
		buffers = new OGLBuffers(vertexBufferDataPos, attributesPos,
				indexBufferData);
	}

	private int init(int tessMode){
		int newShaderProgram = 0;
		if (OGLUtils.getVersionGLSL() >= TESSELATION_SUPPORT_VERSION) {
			//newShaderProgram = ShaderUtils.loadProgram("/tessel/tessel");
			
			if(tessMode == 0) {
				newShaderProgram = ShaderUtils.loadProgram("/tessel/tessel.vert",
						"/tessel/tessel.frag","/tessel/tessel.geom","/tessel/tessel.tesc","/tessel/tessel.tese",null); 
			}
			if(tessMode == 1) {
				newShaderProgram = ShaderUtils.loadProgram("/tessel/tessel.vert",
						"/tessel/tessel.frag","/tessel/tessel.geom","/tessel/tessel.tesc","/tessel/tessel2.tese",null); 
			}
			if(tessMode == 2) {
				newShaderProgram = ShaderUtils.loadProgram("/tessel/tessel.vert",
						"/tessel/tessel.frag","/tessel/tessel.geom","/tessel/tessel.tesc","/tessel/tessel3.tese",null); 
			}
			
			cam = cam.withPosition(new Vec3D(20, 20, 15)).withAzimuth(Math.PI * 1.25).withZenith(Math.PI * -0.140);	
			locMatModel = glGetUniformLocation(newShaderProgram, "model");
			locMatView = glGetUniformLocation(newShaderProgram, "view");
			locMatProj = glGetUniformLocation(newShaderProgram, "proj");
			locTessType = glGetUniformLocation(newShaderProgram, "tessType");
			locColType = glGetUniformLocation(newShaderProgram, "colorType");
		}	
		else
			System.out.println("Tesselation is not supported");
		return newShaderProgram;	
		
	}  
	
	@Override
	public void init() {
		if (OGLUtils.getVersionGLSL() >= TESSELATION_SUPPORT_VERSION) {
			int[] maxPatchVertices = new int[300];
			glGetIntegerv(GL_MAX_PATCH_VERTICES, maxPatchVertices);
		}
		
		cam = cam.withPosition(new Vec3D(20, 20, 15)).withAzimuth(Math.PI * 1.25).withZenith(Math.PI * -0.140);	
		locMatModel = glGetUniformLocation(shaderProgram, "model");
		locMatView = glGetUniformLocation(shaderProgram, "view");
		locMatProj = glGetUniformLocation(shaderProgram, "proj");
		locTessType = glGetUniformLocation(shaderProgram, "tessType");
		locColType = glGetUniformLocation(shaderProgram, "colorType");
		createBuffers();
		textRenderer = new OGLTextRenderer(width, height);
		speed = 20;
	}
	
	@Override
	public void display() {	
		
		if (demoTypeChanged) {
			int oldShaderProgram = shaderProgram;
			shaderProgram = init(tessMode);
			if (shaderProgram>0) {
				glDeleteProgram(oldShaderProgram);
			} else {
				shaderProgram = oldShaderProgram;
			}
			locMatModel = glGetUniformLocation(shaderProgram, "model");
			locMatView = glGetUniformLocation(shaderProgram, "view");
			locMatProj = glGetUniformLocation(shaderProgram, "proj");
			locTime = glGetUniformLocation(shaderProgram, "time");
			locTessType = glGetUniformLocation(shaderProgram, "tessType");
			locColType = glGetUniformLocation(shaderProgram, "colorType");
			demoTypeChanged = false;
		}
		
		glEnable(GL_DEPTH_TEST);
		glLineWidth(lineWidth);
		
		if(!lineFillSwitch){	
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		}
		else{	
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		}
		
		glViewport(0, 0, width, height);
		glClearColor(0.15f, 0.15f, 0.15f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		double plustime = 1 + ((double)speed/4000);
		if (!stop) time *= plustime;
		time =  time % 100;

		if(projSwitch) {
			glUniformMatrix4fv (locMatProj, false,
					projPersp.floatArray());
		}
		else {
			glUniformMatrix4fv (locMatProj, false,
					projOrth.floatArray());
		}
		
		glUniformMatrix4fv (locMatModel, false, new Mat4Scale(5).floatArray());		
		glUniformMatrix4fv (locMatView, false, cam.getViewMatrix().floatArray());	
		
		glUniform1i (locTessType, tessType);
		glUniform1i (locColType, colorType);
		
		glUseProgram(shaderProgram); 
		glUniform1f(locTime, time); 
		
		if (OGLUtils.getVersionGLSL() >= 400){
			glPatchParameteri(GL_PATCH_VERTICES, 4);
			buffers.draw(GL_PATCHES, shaderProgram);
		}

		String text = new String("Pohyb: [W A S D Q E]          Rozhl�en�: [LMB + Mouse]          First person mode: [SPACE]          Dal�� nastaven� v nab�dce");
		
		textRenderer.clear();
		textRenderer.addStr2D(3, 20, text);
		textRenderer.addStr2D(width-140, height-3, " Autor: Tom� Poho�sk�");
		textRenderer.draw();
	}

	public void setProj(boolean proj) {
		this.projSwitch = proj;
	}
	public void setLineFill(boolean liFi) {
		this.lineFillSwitch = liFi;
	}
	public void setLineWidth(int width) {
		this.lineWidth = width;
	}
	public void setMovings(boolean stop) {
		this.stop = stop;
	}
	public void setCol(int col) {
		this.colorType = col;
	}
	public void tessType(int type) {
		this.tessType = type;
		this.time = 1;
	} 
	public void tessMode(int type) {
		this.tessMode = type;
		this.time = 1;
		demoTypeChanged = true;
	} 
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	
}