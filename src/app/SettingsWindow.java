package app;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Panel;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
/**
* T��da obsahuj�c� dodate�n� nastaven�
* @author Tom� Poho�sk�
*/
public class SettingsWindow extends Frame{
	private static final long serialVersionUID = 1L;
	public int lineWidth;
	public JRadioButton orto = new JRadioButton("Ortogon�ln�");  
	public JRadioButton persp = new JRadioButton("Perspektivn�",true);
	public JRadioButton line = new JRadioButton("Dr�tov�", true);  
	public JRadioButton fill = new JRadioButton("Vypln�n�");
	public JSlider slider = new JSlider(JSlider.HORIZONTAL, 1, 20, 2);
	public JRadioButton checkSun = new JRadioButton("Zastavit");
	public JRadioButton checkModel = new JRadioButton("Spustit", true);
	public JRadioButton vol1 = new JRadioButton("Typ 1", true);
	public JRadioButton vol2 = new JRadioButton("Typ 2");
	public JRadioButton vol3 = new JRadioButton("Typ 3");
	public JRadioButton vol4 = new JRadioButton("Typ 4");
	public JRadioButton attN = new JRadioButton("B�l�", true);
	public JRadioButton attY = new JRadioButton("Barevn�");
	public JRadioButton equal = new JRadioButton("Equal-spacing", true);
	public JRadioButton fragEv = new JRadioButton("Fractional-even-spacing");
	public JRadioButton fragOdd = new JRadioButton("Fractional-odd-spacing");
	public JSlider sliderSpeed = new JSlider(JSlider.HORIZONTAL, 1, 30, 20);
	
public SettingsWindow() { 
	
	Panel projectionPanel = new Panel();
	projectionPanel.setBounds(10, 50, 270, 50);
	projectionPanel.setBackground(Color.LIGHT_GRAY);
	JLabel projection = new JLabel("-----------------Nastaven� projekce-----------------");
	ButtonGroup bg1 = new ButtonGroup();
	bg1.add(orto);
	bg1.add(persp);
	orto.setBackground(Color.LIGHT_GRAY);
	persp.setBackground(Color.LIGHT_GRAY);
	projectionPanel.add(projection);
	projectionPanel.add(persp);	projectionPanel.add(orto);
	this.add(projectionPanel, BorderLayout.NORTH); 
	
	Panel linePanel = new Panel();
	linePanel.setBounds(10, 110, 270, 50);
	linePanel.setBackground(Color.LIGHT_GRAY);
	JLabel lines = new JLabel("-------------Dr�tov�/vypln�n� model-------------");
	ButtonGroup bg2 = new ButtonGroup();
	bg2.add(line);
	bg2.add(fill);
	line.setBackground(Color.LIGHT_GRAY);
	fill.setBackground(Color.LIGHT_GRAY);
	linePanel.add(lines);
	linePanel.add(line);	linePanel.add(fill);
	this.add(linePanel, BorderLayout.NORTH); 
	
	Panel lightPanel = new Panel();
	lightPanel.setBounds(10, 170, 270, 50);
	lightPanel.setBackground(Color.LIGHT_GRAY);
	JLabel tessType = new JLabel("-----------------Typ teselace-----------------");
	ButtonGroup bg3 = new ButtonGroup();
	bg3.add(vol1);
	bg3.add(vol2);
	bg3.add(vol3);
	bg3.add(vol4);
	vol1.setBackground(Color.LIGHT_GRAY);
	vol2.setBackground(Color.LIGHT_GRAY);
	vol3.setBackground(Color.LIGHT_GRAY);
	vol4.setBackground(Color.LIGHT_GRAY);
	lightPanel.add(tessType);
	lightPanel.add(vol1);	lightPanel.add(vol2);
	lightPanel.add(vol3);	lightPanel.add(vol4);
	this.add(lightPanel, BorderLayout.NORTH); 
	
	Panel widthPanel = new Panel();
	widthPanel.setBounds(10, 230, 270, 50);
	widthPanel.setBackground(Color.LIGHT_GRAY);
	JLabel width = new JLabel("------------------Tlou��ka ��ry-------------------");
	slider.setMajorTickSpacing(1);  
	slider.setPaintTicks(false);  
	slider.setPaintLabels(false); 
	slider.setBackground(Color.LIGHT_GRAY);
	lineWidth = slider.getValue();
	widthPanel.add(width);
	widthPanel.add(slider);	
	this.add(widthPanel, BorderLayout.NORTH); 
	
	Panel movePanel = new Panel();
	movePanel.setBounds(10, 290, 270, 50);
	movePanel.setBackground(Color.LIGHT_GRAY);
	JLabel move = new JLabel("--------------------Animace---------------------");
	ButtonGroup bg4 = new ButtonGroup();
	bg4.add(checkSun);
	bg4.add(checkModel);
	checkSun.setBackground(Color.LIGHT_GRAY);
	checkModel.setBackground(Color.LIGHT_GRAY);
	movePanel.add(move);
	movePanel.add(checkSun);  movePanel.add(checkModel);	
	this.add(movePanel, BorderLayout.NORTH); 
	
	Panel att = new Panel();
	att.setBounds(10, 350, 270, 50);
	att.setBackground(Color.LIGHT_GRAY);
	JLabel attL = new JLabel("----------------Nastaven� barvy-----------------");
	attN.setBackground(Color.LIGHT_GRAY);
	attY.setBackground(Color.LIGHT_GRAY);
	att.add(attL);
	att.add(attY);  att.add(attN);	
	ButtonGroup bg8 = new ButtonGroup();
	bg8.add(attN);
	bg8.add(attY);
	this.add(att, BorderLayout.NORTH); 
	
	Panel mode = new Panel();
	mode.setBounds(10, 410, 270, 120);
	mode.setBackground(Color.LIGHT_GRAY);
	JLabel modeL = new JLabel("-----------------Mod teselace------------------");
	equal.setBackground(Color.LIGHT_GRAY);
	fragEv.setBackground(Color.LIGHT_GRAY);
	fragOdd.setBackground(Color.LIGHT_GRAY);
	mode.add(modeL);
	mode.add(equal);  mode.add(fragEv); mode.add(fragOdd);
	ButtonGroup bg9 = new ButtonGroup();
	bg9.add(equal);
	bg9.add(fragEv);
	bg9.add(fragOdd);
	this.add(mode, BorderLayout.NORTH); 
	
	Panel speedPanel = new Panel();
	speedPanel.setBounds(10, 540, 270, 50);
	speedPanel.setBackground(Color.LIGHT_GRAY);
	JLabel speed = new JLabel("-------------------Rychlost--------------------");
	sliderSpeed.setBackground(Color.LIGHT_GRAY);
	speedPanel.add(speed);
	speedPanel.add(sliderSpeed);	
	this.add(speedPanel, BorderLayout.NORTH); 
	
	
	this.setLayout(null);    
	this.setBackground(Color.DARK_GRAY);

	addWindowListener(new WindowAdapter() {
		@Override
		public void windowClosing(WindowEvent e) {
			new Thread() {
				@Override
				public void run() {
					System.exit(0);
				}
			}.start();
		}
	});
	
	pack();
	setVisible(true);
	setResizable(false);
	}

}

