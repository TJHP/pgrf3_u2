package app;

import static org.lwjgl.glfw.GLFW.GLFW_COCOA_RETINA_FRAMEBUFFER;
import static org.lwjgl.glfw.GLFW.GLFW_FALSE;
import static org.lwjgl.glfw.GLFW.GLFW_RESIZABLE;
import static org.lwjgl.glfw.GLFW.GLFW_TRUE;
import static org.lwjgl.glfw.GLFW.GLFW_VISIBLE;
import static org.lwjgl.glfw.GLFW.glfwDefaultWindowHints;
import static org.lwjgl.glfw.GLFW.glfwInit;
import static org.lwjgl.glfw.GLFW.glfwSetErrorCallback;
import static org.lwjgl.glfw.GLFW.glfwTerminate;
import static org.lwjgl.glfw.GLFW.glfwWaitEvents;
import static org.lwjgl.glfw.GLFW.glfwWindowHint;
import static org.lwjgl.glfw.GLFW.glfwWindowShouldClose;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.system.Platform;

/**
* Spou�t�c� t��da 2. �lohy PGRF3 2019
* @author Tom� Poho�sk�
*/
public class App {
	
	@SuppressWarnings("unused")
	private int demoId = 1;

	public boolean proj;
	
	LwjglWindowTess thread;
	SettingsWindow mainWindow;
	CountDownLatch quit;
	long window = 0;

	private Integer setApp() {
		if (thread != null) {
			try {
				thread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}			
			thread.dispose();
		}
		quit = new CountDownLatch(1);
		thread = new LwjglWindowTess(0, quit, new app.Renderer());
		window = thread.getWindow();
		thread.start();
		
		
		mainWindow.orto.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {

				thread.setProj(false);
				
			}
		});
		mainWindow.persp.addActionListener(new ActionListener() {		
			@Override
			public void actionPerformed(ActionEvent e) {
				thread.setProj(true);
				
			}
		});
		
		mainWindow.line.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				thread.setLineFill(false);			
			}
		});
		mainWindow.fill.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				thread.setLineFill(true);			
			}
		});	
		mainWindow.slider.addChangeListener(new ChangeListener() {		
			@Override
			public void stateChanged(ChangeEvent e) {
				thread.setLineWidth(mainWindow.slider.getValue());		
			}
		});
		mainWindow.checkModel.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				thread.setMovings(false);			
			}
		});
		mainWindow.checkSun.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				thread.setMovings(true);			
			}
		});	
		mainWindow.attY.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(mainWindow.attY.isSelected()) {
					thread.setCol(1);
				}			
			}
		});
		mainWindow.attN.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(mainWindow.attN.isSelected()) {
					thread.setCol(0);
				}			
			}
		});  
		
		mainWindow.vol1.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				thread.tessType(0);			
			}
		});
		mainWindow.vol2.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				thread.tessType(1);			
			}
		});	
		mainWindow.vol3.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				thread.tessType(2);			
			}
		});
		mainWindow.vol4.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				thread.tessType(3);			
			}
		});	
		
		mainWindow.equal.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				thread.tessMode(0);			
			}
		});
		mainWindow.fragEv.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				thread.tessMode(1);			
			}
		});		
		mainWindow.fragOdd.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				thread.tessMode(2);			
			}
		});
		
		mainWindow.sliderSpeed.addChangeListener(new ChangeListener() {		
			@Override
			public void stateChanged(ChangeEvent e) {
				thread.setSpeed(mainWindow.sliderSpeed.getValue());		
			}
		});

		
		return null;
	}
	public void start() {
		SwingUtilities.invokeLater(new Runnable() {
	        public void run() {
	            mainWindow = new SettingsWindow();
	            mainWindow.setTitle("Nastaven� U2");
	            mainWindow.setSize(290, 800);
	            mainWindow.setVisible(true);
	        }
		});
		GLFWErrorCallback.createPrint(System.err).set();
		if (!glfwInit()) throw new IllegalStateException("Unable to initialize GLFW");

		glfwDefaultWindowHints(); 
		glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
		glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
		
		if (Platform.get() == Platform.MACOSX) {
			glfwWindowHint(GLFW_COCOA_RETINA_FRAMEBUFFER, GLFW_FALSE);
		}
		
		setApp();
		
		out: while (true) {
			glfwWaitEvents();
			if (window > 0 && glfwWindowShouldClose(window)) {
				quit.countDown();
				break out;
			}
		}

		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		thread.dispose();

		glfwTerminate();
		Objects.requireNonNull(glfwSetErrorCallback(null)).free();
		mainWindow.setVisible(false);
		mainWindow.dispose();
		System.exit(0);
	}

	public static void main(String[] args) {
		new App().start();
		    
	}

}